package main

import (
	"fmt"
	"net/http"
	"text/template"
)

func index(w http.ResponseWriter, r *http.Request) {
	//定义模板
	//解析模板
	t, err := template.New("index.tmpl").
		Delims("{[", "]}").
		ParseFiles("./index.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
	}
	msg := "index.page"
	//渲染模板
	err01 := t.Execute(w, msg)
	if err01 != nil {
		return
	}

}
func xss(w http.ResponseWriter, r *http.Request) {
	//定义模板
	//解析模板
	//解析模板之前定义一个自定义函数sage
	t, err := template.New("xss.tmpl").Funcs(template.FuncMap{
		"safe": func(str string) string {
			return template.HTMLEscapeString(str)
		},
	}).ParseFiles("./xss.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
		return
	}
	//渲染模板
	str1 := "<script>alert(\"你好\");</script>"
	str2 := "<a href='https://www.baidu.com' blank>baidu</a>"
	err01 := t.Execute(w, map[string]string{
		"str1": str1,
		"str2": str2,
	})
	if err01 != nil {
		return
	}
}
func main() {
	http.HandleFunc("/index", index)
	http.HandleFunc("/xss", xss)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("serve err:", err)
		return
	}
}
