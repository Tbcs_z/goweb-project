package main

import (
	"fmt"
	"net/http"
	"text/template"
)

type UserInfo struct {
	Name string
	Sex  bool
	Age  int
}

func f1(w http.ResponseWriter, r *http.Request) {
	//定义一个函数kua
	kua := func(name string) string {
		return name + "最牛逼！"
	}
	//定义模板
	t := template.New("f.tmpl") //name 要和解析的模板名字对应
	//告诉模板引擎，现在多了一个自定义的函数kua
	t.Funcs(template.FuncMap{
		"k": kua,
	})

	//解析模板
	_, err := t.ParseFiles("./f.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
	}
	name := "小明"
	//渲染模板
	t.Execute(w, name)
}
func demo01(w http.ResponseWriter, r *http.Request) {
	//定义模板
	//解析模板
	t, err := template.ParseFiles("./t.tmpl", "./ul.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
	}
	name := "小明"
	//渲染模板
	t.Execute(w, name)
}
func main() {
	http.HandleFunc("/", f1)
	http.HandleFunc("/tmpl", demo01)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("serve err:", err)
		return
	}
}
