package main

import (
	"fmt"
	"net/http"
	"text/template"
)

func index(w http.ResponseWriter, r *http.Request) {
	//定义模板
	//解析模板
	t, err := template.ParseFiles("./index.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
	}
	msg := "index.page"
	//渲染模板
	t.Execute(w, msg)

}
func home(w http.ResponseWriter, r *http.Request) {
	//定义模板
	//解析模板
	t, err := template.ParseFiles("./home.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
	}
	msg := "home.page"
	//渲染模板
	t.Execute(w, msg)

}
func index2(w http.ResponseWriter, r *http.Request) {
	//定义模板
	//解析模板
	t, err := template.ParseFiles("./templates/base.tmpl", "./templates/index2.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
	}
	msg := "index2.page"
	//渲染模板
	t.ExecuteTemplate(w, "index2.tmpl", msg)

}
func home2(w http.ResponseWriter, r *http.Request) {
	//定义模板
	//解析模板
	t, err := template.ParseFiles("./templates/base.tmpl", "./templates/home2.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
	}
	msg := "home2.page"
	//渲染模板
	t.ExecuteTemplate(w, "home2.tmpl", msg)

}
func main() {
	http.HandleFunc("/index", index)
	http.HandleFunc("/home", home)
	http.HandleFunc("/index2", index2)
	http.HandleFunc("/home2", home2)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("serve err:", err)
		return
	}
}
