package main

import (
	"fmt"
	"net/http"
	"text/template"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	//2、解析模板
	t, err := template.ParseFiles("./ul.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
		return
	}
	//3、渲染模板
	name := "golang"
	exerr := t.Execute(w, name)
	if exerr != nil {
		fmt.Println("execute err:", err)
		return
	}
}
func main() {
	http.HandleFunc("/", sayHello)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("serve err:", err)
		return
	}
}
