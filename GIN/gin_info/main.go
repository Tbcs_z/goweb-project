package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserInfo struct {
	Username string `form:“username” json:"user"`
	Password string `form:”password“ json:"pwd"`
}

func main() {
	r := gin.Default()
	r.GET("/user", func(c *gin.Context) {
		//username := c.Query("username")
		//password := c.Query("password")
		//U := UserInfo{
		//	Username: username,
		//	Password: password,
		//}
		var u UserInfo //声明一个userinfo类型的变量u
		err := c.ShouldBind(&u)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			fmt.Println(u)
			c.JSON(http.StatusOK, gin.H{
				"message": "ok",
			})
		}

	})
	r.POST("/login", func(c *gin.Context) {
		var u UserInfo //声明一个userinfo类型的变脸u
		//ShouldBind()会根据请求的Content-Type自行选择绑定器
		err := c.ShouldBind(&u)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			fmt.Println(u)
			c.JSON(http.StatusOK, gin.H{
				"message": "ok",
			})
		}
	})
	r.Run(":9090")
}
