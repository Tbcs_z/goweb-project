package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

/*
Gin看框架允许开发者在处理请求的过程中，加入用户子的钩子(HOOK)函数。这个钩子函数就叫
中间件，中间件适合处理一些公共的业务逻辑，比如登录认证、权限校验、数据分页、记录日志、耗时统计等
*/
//handlerFunc
func indexHandler(c *gin.Context) {
	fmt.Println("index")
	name, ok := c.Get("name") //从上下文中取值（跨中间件存取值）
	if !ok {
		name = "匿名用户"
	}
	c.JSON(http.StatusOK, gin.H{
		"msg": name,
	})
}

// 定义一个中间件m1,统计请求处理函数的耗时
func m1(c *gin.Context) {
	fmt.Println("m1 in...")
	//计时
	start := time.Now()
	//go funcxx(c.Copy()) //在中只能使用c的拷贝
	c.Next() //调用后续的处理函数
	//c.Abort() //阻止调用后续的处理函数
	cost := time.Since(start)
	fmt.Println("cost:", cost)
	fmt.Println("m1 out...")
}
func m2(c *gin.Context) {
	fmt.Println("m2 in...")
	c.Set("name", "xiaoz")

	c.Next() //调用后续的处理函数
	fmt.Println("m2 out...")
}
func authMiddleWare(doCheck bool) gin.HandlerFunc {
	//连接数据库
	//或做一些其他准备工作
	return func(c *gin.Context) {
		if doCheck {
			//存放具体逻辑
			//是否登录判断
			//if 是 则登录用户
			c.Next()
			//else
			//c.abort()
		} else {
			c.Next()
		}
	}
}
func main() {
	//r := gin.Default() //默认使用Logger和Recovery中间件
	r := gin.New() //如果不想使用上述两个中间件，则使用gin.New()
	r.Use(m1, m2)  //全局注册中间件函数m1
	r.GET("index", indexHandler)
	r.GET("/shop", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "shop",
		})
	})
	r.GET("/user", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "user",
		})
	})
	//路由组中间件方法1：
	group1 := r.Group("/xx", authMiddleWare(true))
	{
		group1.GET("/test1", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group",
			})
		})
		group1.GET("/test2", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group",
			})
		})
	}
	//路由组中间件方法2：
	group2 := r.Group("/xx2")
	group2.Use(authMiddleWare(true))
	{
		group2.GET("/test1", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group2",
			})
		})
		group2.GET("/test2", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group2",
			})
		})
	}
	r.Run()
}
