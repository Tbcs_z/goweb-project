package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// 获取form表单提交的数据
func main() {
	r := gin.Default()
	r.LoadHTMLFiles("./login.html", "./index.html")
	r.GET("/login", func(c *gin.Context) {
		c.HTML(http.StatusOK, "login.html", nil)

	})
	r.POST("/login", func(c *gin.Context) {
		//获取表单提交的数据
		//第一种
		//username := c.PostForm("username")
		//pwd := c.PostForm("pwd")
		//第二种
		//username := c.DefaultPostForm("username", "default")
		//pwd := c.DefaultPostForm("xxx", "test")
		//第三种
		username, ok := c.GetPostForm("username")
		if !ok {
			username = "sb"
		}
		pwd, ok1 := c.GetPostForm("pwd")
		if !ok1 {
			pwd = "test"
		}
		c.HTML(http.StatusOK, "index.html", gin.H{
			"Name": username,
			"Pwd":  pwd,
		})
	})
	r.Run(":9090")
}
