package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	r.GET("web", func(c *gin.Context) {
		//获取浏览器发送的请求携带的参数（query string）
		//多个key-value 用 & 连接 如：127.0.0.1/web?query=xx&age=1
		name := c.Query("query") //通过Query获取请求中携带的参数
		age := c.Query("age")
		//name := c.DefaultQuery("query", "none Result") //通过Query获取请求中携带的参数
		//name, ok := c.GetQuery("query")
		//if !ok {
		//	name = "none"
		//}
		c.JSON(http.StatusOK, gin.H{
			"name": name,
			"age":  age,
		})
	})
	r.Run(":9090")
}
