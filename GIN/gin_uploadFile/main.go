package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"path"
)

func main() {
	r := gin.Default()
	r.LoadHTMLFiles("./index.html")
	r.GET("/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	r.POST("/upload", func(c *gin.Context) {
		//1、从请求中读取文件，单个文件上传
		//处理multipart forms 提交文件时默认的内存限制是32MiB
		//可以通过下面的方式修改
		//router.MaxMultipartMemory = 8 << 20 // 8MiB
		file, err := c.FormFile("f1")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
		} else {
			//将读取到的文件保存在服务端本地
			//dst := fmt.Sprintf("./%s", file.Filename)
			dst := path.Join("./", file.Filename) //接收多个字符串拼接返回一个字符串
			err := c.SaveUploadedFile(file, dst)
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"message": err.Error(),
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status": "ok",
				})
			}
		}
	})
	r.Run(":9090")
}
