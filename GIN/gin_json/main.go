package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	r.GET("/json", func(c *gin.Context) {
		//方法1：使用map
		//data := map[string]interface{}{
		//	"name":    "小王子",
		//	"message": "hello world",
		//	"age":     18,
		//}
		//gin内置声明方式
		data := gin.H{
			"name":    "小王子",
			"message": "hello world",
			"age":     18,
		}
		c.JSON(http.StatusOK, data)
	})
	//方法2 ，结构体,可使用tag来对字段做定制化操作
	type msg struct {
		Name    string `json:"name"`
		Age     int    `json:"age"`
		Message string `json:"message"`
	}
	r.GET("/json2", func(c *gin.Context) {
		data := msg{
			"小明",
			18,
			"nihao!",
		}
		c.JSON(http.StatusOK, data)
	})

	r.Run(":9090")
}
