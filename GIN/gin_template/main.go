package main

import (
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
	template2 "html/template"
	"net/http"
	"path/filepath"
	"text/template"
)

// Gin框架默认都是使用单模板，如果需要使用block template功能，可以通过"github.com/gin-contrib/multitemplate"库实现
// 定义一个loadTemplates
func loadTemplates(templatesDir string) multitemplate.Renderer {
	r := multitemplate.NewRenderer()
	posts, err := filepath.Glob(templatesDir + "/posts/*.tmpl")
	if err != nil {
		panic(err.Error())
	}
	users, err := filepath.Glob(templatesDir + "/users/*.tmpl")
	if err != nil {
		panic(err.Error())
	}
	// 为layouts/和includes/目录生成 templates map
	for _, include := range users {
		layoutCopy := make([]string, len(posts))
		copy(layoutCopy, posts)
		files := append(layoutCopy, include)
		r.AddFromFiles(filepath.Base(include), files...)
	}
	return r
}

//静态文件：HTML、css、js、图片

func main() {
	r := gin.Default()
	//加载静态文件
	r.Static("/xxx", "./statics")
	//gin框架中给模板添加自定义函数
	r.SetFuncMap(template.FuncMap{
		"safe": func(str string) template2.HTML {
			return template2.HTML(str)
		},
	})
	//r.LoadHTMLFiles("./templates/index.html", "./templates/posts/index.html", "./templates/users/index.html") //模板解析
	r.LoadHTMLGlob("templates/**/*")
	r.GET("/posts/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "posts/index.tmpl", gin.H{
			"title": "<a href=\"https://www.nbclass.icu\">posts/index<a>",
		})
	})

	r.GET("users/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "users/index.tmpl", gin.H{
			"title": "<script>users/index</script>",
		})
	})
	r.GET("index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	r.GET("standalone", func(c *gin.Context) {
		c.HTML(http.StatusOK, "standalone.html", nil)
	})
	err := r.Run(":9090") //启动server
	if err != nil {
		return
	}
}
