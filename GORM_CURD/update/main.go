package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"golang.org/x/crypto/bcrypt"
)

// 1、定义模型
type User struct {
	gorm.Model
	Name     string
	Age      int64
	Password []byte
	Active   bool
}

func main() {
	//连接mysql数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormdb?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	//2、把模型与数据库中的表对应起来
	db.AutoMigrate(&User{})

	//3、创建
	//u1 := User{
	//	Name:   "xiaozhu",
	//	Age:    18,
	//	Active: true,
	//}
	//db.Create(&u1)
	//u2 := User{
	//	Name:   "maogou",
	//	Age:    13,
	//	Active: false,
	//}
	//db.Create(&u2)
	//4、查询
	var user User
	db.First(&user)
	var users []User
	db.Find(&users)
	//5、更新
	user.Name = "xiaoai"
	user.Age = 29
	db.Debug().Save(&user) //save默认会修改所有字段
	//UPDATE `users` SET `created_at` = '2022-11-27 13:56:23', `updated_at` = '2022-12-04 16:07:20', `deleted_at` = NULL, `name` = 'xiaoai', `age` = 29, `active` = true  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 5
	////
	////更新单个属性
	//db.Model(&user).Update("name", "woshinidie")
	////根据给定的条件更新单个属性
	//db.Model(&users).Debug().Where("active=?", false).Update("active", true)
	////使用map更新多个属性，只会更新其中有变化的属性
	//db.Model(&user).Updates(map[string]interface{}{"name": "hello", "age": 18, "active": false})
	////使用struct更新多个属性，只会更新其中有变化且为非零值的字段
	//db.Model(&user).Updates(User{
	//	Name:   "Xixi",
	//	Age:    21,
	//	Active: true,
	//})
	////注意：当使用struct更新时，GORM只会更新那些非零值的字段
	////对于下面的操作，不会发生任何更新，"",0,false 都是其类型的零值
	//db.Model(&user).Updates(User{
	//	Name:   "",
	//	Age:    0,
	//	Active: false,
	//})
	////更新选定字段
	////如果想更新或忽略某些字段，可以使用Select , Omit
	////指定更新
	//db.Model(&user).Select("name").Updates(map[string]interface{}{"name": "小艾同学", "age": 18, "active": false})
	////忽略更新
	//db.Model(&user).Omit("name").Updates(map[string]interface{}{"name": "小艾同学", "age": 18, "active": false})

	//无Hooks更新
	//上面的更新操作会自动运行model的BeforeUpdate，AfterUpdate 方法，更新UpdateAt时间戳，
	//在更新时保存其 Associations，如果你不想调用这些方法，你可以使用UpdateColumn，UpdateColumns
	//更新单个属性，类似于 'Update'
	//db.Model(&user).UpdateColumn("name", "hello")
	////更新多个属性，类似于 'Updates'
	//db.Model(&user).UpdateColumns(User{
	//	Name: "Neo",
	//	Age:  15,
	//})

	//批量更新
	//db.Table("users").Where("id IN(?)", []int{1, 2}).Updates(map[string]interface{}{"name": "hello", "age": 12})
	////使用struct更新时，只会更新非零值字段，若想更新所有字段，请使用map[string]interface{}
	//db.Model(User{}).Updates(User{
	//	Name:   "hello",
	//	Age:    18,
	//	Active: false,
	//})
	////使用 'RowsAffected' 获取更新记录总数
	//rows := db.Model(User{}).Updates(User{Name: "hello", Age: 19}).RowsAffected
	//fmt.Println(rows)

	//使用SQL语句更新
	//先查询表中的第一条数据保存至user
	//var user User
	//db.First(&user)
	db.Model(&user).Debug().Update("age", gorm.Expr("age * ? + ?", 2, 100))
	//UPDATE `users` SET `age` = age * 2 + 100, `updated_at` = '2022-12-04 16:40:16'  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 5
	db.Model(&user).Debug().Updates(map[string]interface{}{"age": gorm.Expr("age * ? + ?", 2, 100)})
	//UPDATE `users` SET `a ge` = age * 2 + 100, `updated_at` = '2022-12-04 16:49:23'  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 5
	db.Model(&user).Debug().UpdateColumn("age", gorm.Expr("age - ?", 1))
	//UPDATE `users` SET `age` = age - 1  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 5
	db.Model(&user).Debug().Where("age > 10").UpdateColumn("age", gorm.Expr("age - ?", 1))
	//UPDATE `users` SET `age` = age - 1  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 5 AND ((age > 10))
	//为 updage SQL 添加其他的SQL
	db.Model(&user).Set("gorm:update_option", "OPTION(OPTIMIZE FOR UNKNOWN)").Update("name", "hello")
}

// 修改Hooks中的值
// 如果想修改 BeforeUpdate ,BeforeSave 等 Hooks 中更新的值，可以使用 scope.SetColumn ,例如：
func (u User) BeforeSave(scope *gorm.Scope) (err error) {
	if pw, err := bcrypt.GenerateFromPassword(u.Password, 0); err == nil {
		err := scope.SetColumn("EncryptedPassword", pw)
		if err != nil {
			return err
		}
	}
	return nil
}
