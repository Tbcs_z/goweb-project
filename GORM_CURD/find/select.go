package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// 1、定义模型
type User struct {
	gorm.Model
	Name string
	Age  int64
}

func main() {
	//连接mysql数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormdb?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	//2、把模型与数据库中的表对应起来
	db.AutoMigrate(&User{})

	//3、创建
	//u1 := User{
	//	Name: "xiaozhu",
	//	Age:  18,
	//}
	//db.Create(&u1)
	//u2 := User{
	//	Name: "maogou",
	//	Age:  13,
	//}
	//db.Create(&u2)
	//4、查询
	var user User //声明模型结构体类型变量user （文件夹A）
	//user := new(User) //new和make的区别
	//根据主键查询第一条记录
	//db.First(&user)
	//select * from users order by id limit 1;
	var users []User
	//查询所有的记录
	//db.Debug().Find(&users)
	//随机获取一条记录
	//db.Take(&user)
	//获取最后一条记录
	//db.Last(user)
	//根据主键检索
	//db.First(&user, 2) //select * from users where id = 2;
	//db.First(&user, "2")
	db.Find(&users, []int{1, 2}) //select * from users where id in (1,2);
	fmt.Println(users)
	//当目标对象有一个主要值时，将使用主键构建条件，例如：
	user.ID = 2
	db.First(&user)
	fmt.Println(user)
}
