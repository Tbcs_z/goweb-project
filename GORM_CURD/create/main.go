package main

import (
	"database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// 1、定义模型
type User struct {
	ID   int64
	Name sql.NullString `gorm:"default:'NoName'"`
	Age  sql.NullInt64  `gorm:"default:1"`
}

func main() {
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormdb?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	//2、把模型与数据库中的表对应起来
	db.AutoMigrate(&User{})
	//3、创建记录
	u := User{
		Name: sql.NullString{String: "", Valid: true},
		Age:  sql.NullInt64{Int64: 0, Valid: true},
	} //在代码层面创建一个user对象
	//1、使用指针的方式将零值存入数据库
	//u1 := User{Name: new(string), Age: 29}
	//2、使用sql.NUllString
	//u1 := User{Name: sql.NullString{String: "", Valid: true}, Age: 18}
	fmt.Println(db.NewRecord(&u)) //判断主键是否为空
	db.Create(&u)
	//db.Create(&u1)
	fmt.Println(db.NewRecord(&u)) //判断主键是否为空
}
