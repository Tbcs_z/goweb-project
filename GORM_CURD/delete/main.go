package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// 1、定义模型
type User struct {
	gorm.Model
	Name     string
	Age      int64
	Password []byte
	Active   bool
}

func main() {
	//连接mysql数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormdb?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	//2、把模型与数据库中的表对应起来
	db.AutoMigrate(&User{})
	//3、创建
	//u1 := User{
	//	Name:   "xiaozhu",
	//	Age:    18,
	//	Active: true,
	//}
	//db.Create(&u1)
	//u2 := User{
	//	Name:   "maogou",
	//	Age:    13,
	//	Active: false,
	//}
	//db.Create(&u2)
	//4、查询
	var user User
	user.ID = 6
	//删除现有记录(软删除，记录删除时间从而保护数据)
	//db.Debug().Delete(&user)
	//UPDATE `users` SET `deleted_at`='2022-12-08 12:43:45'  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 6
	//为删除SQL添加额外的SQL操作
	//db.Set("gorm:delete_option", "OPTION(OPTIMIZE FOR UNKNOWN)").Delete(&user)
	//批量删除
	//删除全部匹配的数据
	db.Where("name=?", "hello").Delete(User{})
	//db.Delete(User{}, "age = ?", 18)
	//查询被软删除的记录
	db.Unscoped().Where("name=?", "hello").Find(&user)
	fmt.Println(user)
	//物理删除
	db.Unscoped().Delete(&user)
}
