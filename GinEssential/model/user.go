package model

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model `json:"gorm.Model"`
	Name       string `gorm:"type:varchar(20);not null" json:"name"`
	Password   string `gorm:"type:varchar(110);not null" json:"password"`
	Telephone  string `gorm:"type:varchar(11);not null;unique" json:"telephone"`
}
