package main

import (
	"GinEssential/dao"
	"GinEssential/model"
	"GinEssential/routers"
	"github.com/spf13/viper"
	"os"
)

func InitConfig() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir + "/config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {
	InitConfig()
	//连接数据库
	err := dao.InitMysql()
	if err != nil {
		return
	}
	//模型绑定
	dao.DB.AutoMigrate(&model.User{})
	defer dao.DBclose()
	r := routers.SetUpRouter()
	port := viper.GetString("server.port")
	if port != "" {
		panic(r.Run(":" + port))
	}
	panic(r.Run())
}
