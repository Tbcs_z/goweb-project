package dao

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/spf13/viper"
)

var (
	DB *gorm.DB
)

func InitMysql() (err error) {
	UserName := viper.GetString("datasource.username")
	Password := viper.GetString("datasource.password")
	IpAddress := viper.GetString("datasource.host")
	Port := viper.GetString("datasource.port")
	DbName := viper.GetString("datasource.database")
	Charset := viper.GetString("datasource.charset")
	driverName := viper.GetString("datasource.driverName")
	dsn := fmt.Sprintf("%v:%v@(%v:%v)/%v?charset=%v&parseTime=true&loc=Local",
		UserName,
		Password,
		IpAddress,
		Port,
		DbName,
		Charset)
	DB, err = gorm.Open(driverName, dsn)
	if err != nil {
		return err
	}
	return DB.DB().Ping()
}

func DBclose() {
	err := DB.Close()
	if err != nil {
		panic(err)
	}
}
