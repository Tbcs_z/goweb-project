package middleware

import (
	"GinEssential/dao"
	"GinEssential/model"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func AuthMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		//获取Authorization header
		tokenString := c.GetHeader("Authorization")

		//validate token format
		//strings.HasPrefix 测试字符串是否以指定前缀开头
		//此处踩坑：验证Token前缀时，不断401报错，后发现postman默认前缀为Authorization: Bearer ，故此处前缀应为 " Bearer "，bearer前后均有空格
		if tokenString == "" || strings.HasPrefix(tokenString, " Bearer ") {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": 401,
				"msg":  "权限不足",
			})
			//将本次请求抛弃
			c.Abort()
			return
		}
		tokenString = tokenString[7:]
		token, claims, err := dao.ParseToken(tokenString)
		if err != nil || !token.Valid {
			//此处踩坑：token.Valid 应取反，返回值为真时，token验证成功
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": 401,
				"msg":  "权限不足",
				"err":  err,
			})
			//将本次请求抛弃
			c.Abort()
			return
		}
		//验证通过后获取claim中的userId
		userId := claims.UserId
		var user model.User
		dao.DB.First(&user, userId)
		//用户不存在
		if user.ID == 0 {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": 401,
				"msg":  "用户不存在",
			})
			//将本次请求抛弃
			c.Abort()
			return
		}
		//用户存在，将用户信息写入下文
		c.Set("user", user)
		c.Next()
	}
}
