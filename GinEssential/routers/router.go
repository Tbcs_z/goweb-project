package routers

import (
	"GinEssential/controller"
	"GinEssential/middleware"
	"github.com/gin-gonic/gin"
)

func SetUpRouter() *gin.Engine {
	r := gin.Default()

	UserGroup := r.Group("user")
	{
		//用户注册
		UserGroup.POST("/register", controller.Register)
		//用户登录
		UserGroup.POST("/login", controller.Login)
		//用户信息
		UserGroup.GET("/info", middleware.AuthMiddleWare(), controller.Info)
	}
	return r
}
