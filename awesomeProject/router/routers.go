package router

import (
	"awesomeProject/controller"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	//声明模板文件引用的静态文件路径
	r.Static("static", "static")
	//声明模板文件路径
	r.LoadHTMLGlob("templates/*")
	r.GET("/", controller.IndexHandler)
	v1Group := r.Group("v1")
	{
		//待办事项
		//添加
		v1Group.POST("/todo", controller.CreateTask)
		//查看
		//查看所有待办事项
		v1Group.GET("/todo", controller.ShowTasks)
		//修改
		v1Group.PUT("/todo/:id", controller.UpdateTask)
		//删除
		v1Group.DELETE("/todo/:id", controller.DeleteTask)
	}
	return r
}
