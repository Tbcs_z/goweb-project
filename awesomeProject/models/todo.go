package models

import "awesomeProject/dao"

type Dist struct {
	ID     int64  `json:"id"`
	Title  string `json:"title"`
	Status bool   `json:"status"`
}

/*
	Todo 增删改查
	dist这个model的增删改查都在这里
*/

func CreateTask(d *Dist) (err error) {
	return dao.DB.Create(&d).Error
}
func ShowTasks() (d []*Dist, err error) {
	if err := dao.DB.Find(&d).Error; err != nil {
		return nil, err
	}
	return d, err
}
func GetTask(id string) (task *Dist, err error) {
	task = new(Dist)
	if err := dao.DB.Where("id=?", id).First(&task).Error; err != nil {
		return nil, err
	}
	return task, nil
}
func UpdateTask(d *Dist) (err error) {
	return dao.DB.Save(&d).Error
}
func DeleteTask(id string) (err error) {
	return dao.DB.Where("id=?", id).Delete(&Dist{}).Error
}
