package main

import (
	"awesomeProject/dao"
	"awesomeProject/models"
	"awesomeProject/router"
)

func main() {
	//连接数据库
	err := dao.InitMySQL()
	if err != nil {
		panic(err)
	}
	//模型绑定
	dao.DB.AutoMigrate(&models.Dist{})
	//程序退出时关闭连接
	defer dao.Close()
	//注册路由
	r := router.SetupRouter()
	r.Run()
}
