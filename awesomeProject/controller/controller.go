package controller

import (
	"awesomeProject/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

/*
	url --> controller --> service --> model
	请求 -->   控制器    --> 业务逻辑 --> 模型层的增删改查
*/

func IndexHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", nil)
}
func CreateTask(c *gin.Context) {
	//前端页面填写待办事项，点击提交，方式请求到这里
	//1、从请求中把数据拿出来
	var list models.Dist
	c.BindJSON(&list)
	//2、存入数据库
	err := models.CreateTask(&list)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
		return
	} else {
		c.JSON(http.StatusOK, list)
		//c.JSON(http.StatusOK, gin.H{
		//	"status": 200,
		//	"msg":    "successful",
		//	"data":   list,
		//})
	}
}
func ShowTasks(c *gin.Context) {
	//查询lists表中的所有数据
	lists, err := models.ShowTasks()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
		return
	} else {
		c.JSON(http.StatusOK, lists)
	}
}
func UpdateTask(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusOK, gin.H{
			"msg": "id不存在",
		})
		return
	}
	list, err := models.GetTask(id)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"msg": "id不存在",
		})
		return
	}
	c.BindJSON(&list)
	if err = models.UpdateTask(list); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
		return
	} else {
		c.JSON(http.StatusOK, list)
	}
}
func DeleteTask(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusOK, gin.H{
			"msg": "id不存在",
		})
		return
	}
	if err := models.DeleteTask(id); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
		return
	} else {
		c.JSON(http.StatusOK, gin.H{
			"msg": "删除成功！",
		})
	}
}
