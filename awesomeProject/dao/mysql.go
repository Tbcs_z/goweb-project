package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Dist model

var (
	DB *gorm.DB
)

func InitMySQL() (err error) {
	//创建数据库
	//sql:CREATE DATABASE dist;
	//连接数据库
	dsn := "root:root@(127.0.0.1:3306)/dist?charset=utf8mb4&parseTime=true&loc=Local"
	DB, err = gorm.Open("mysql", dsn)
	if err != nil {
		return err
	}
	return DB.DB().Ping()

}
func Close() {
	DB.Close()
}
