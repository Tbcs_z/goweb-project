package main

import (
	"database/sql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
)

type UserInfo struct {
	ID     uint
	Name   string
	Gender string
	Hobby  string
}

/* gorm.Model
type Model struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}
*/

// User 定义模型
type User struct {
	gorm.Model
	Name         string
	Age          sql.NullInt64 //零值类型
	BirthDay     *time.Time
	Email        string  `gorm:"type:varchar(100);unique_index"` //设置邮箱，且唯一索引
	Role         string  `gorm:"size:255"`                       //设置字段大小为255
	MenberNumber *string `gorm:"unique;not null"`                //设置会员号 member number 唯一且不为空
	Num          int     `gorm:"AUTO_INCREMENT"`                 //设置num为自增类型
	Address      string  `gorm:"index.addr"`                     //给address字段创建名为addr的索引
	IgnoreMe     int     `gorm:"_"`                              //忽略本字段
	//还可以使用 tag 来指定列名： `gorm:"column:column_name"`
}

// 使用 AnimalID 作为主键
type Animal struct {
	AnimalID int64 `gorm:"primary_key"`
	Name     string
	Age      int64
}

// 唯一指定表名
func (Animal) TableName() string {
	return "animal"
}
func main() {
	//GORM还支持更改默认表名称规则
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return "SMS_" + defaultTableName
	}
	//连接MySQL数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormDB?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	//禁用创建表时的 复数表名
	db.SingularTable(true)

	//创建表，自动迁移（把结构体和数据表进行对应）
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Animal{})
	//使用User结构体创建名叫yonghu的表
	db.Table("yonghu").CreateTable(&User{})

}
