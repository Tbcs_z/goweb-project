## net/http
示例代码
```go
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	b, _ := ioutil.ReadFile("./hello.tmpl")
	_, err := fmt.Fprintln(w, string(b))
	if err != nil {
		fmt.Println(err)
		return
	}
}

func main() {
	http.HandleFunc("/hello", sayHello)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("serve err:", err)
		return
	}
}

```
## gin
下载并安装gin ``` go get -u github.com/gin-gonic/gin ```
gin示例
```go
package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	// 创建一个默认的路由引擎
	r := gin.Default()
	// GET：请求方式；/hello：请求的路径
	// 当客户端以GET方法请求/hello路径时，会执行后面的匿名函数
	r.GET("/hello", func(c *gin.Context) {
		// c.JSON：返回JSON格式的数据
		c.JSON(200, gin.H{
			"message": "Hello world!",
		})
	})
	// 启动HTTP服务，默认在0.0.0.0:8080启动服务
	r.Run()
}
```
## text/template
Go语言内置了文本模板引擎 ``` text/template``` 和用于HTML文档的 ```html/template```
1、定义模板文件
2、解析文件
```go
func (t *Template) Parse(src string) (*Template, error)
func ParseFiles(filenames ...string) (*Template, error)
func ParseGlob(pattern string) (*Template, error)
```
3、模板渲染
```go
func (t *Template) Execute(wr io.Writer, data interface{}) error
func (t *Template) ExecuteTemplate(wr io.Writer, name string, data interface{}) error
```
## 模板语法
模板语法都包含在 <code> {{ </code>和 <code>}} </code> 中间，其中<code> {{.}} </code>中的点表示当前对象。
示例代码:
```go
package main

import (
	"fmt"
	"net/http"
	"text/template"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	//2、解析模板
	t, err := template.ParseFiles("./hello.tmpl")
	if err != nil {
		fmt.Println("parse err:", err)
		return
	}
	//3、渲染模板
	name := "golang"
	exerr := t.Execute(w, name)
	if exerr != nil {
		fmt.Println("execute err:", err)
		return
	}
}
func main() {
	http.HandleFunc("/", sayHello)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("serve err:", err)
		return
	}
}

```
修改模板引擎标识符<code> Delims(left,right)</code> ：
```go
t, err := template.New("index.tmpl").
		Delims("{[", "]}").
		ParseFiles("./index.tmpl")
```

## gin语法
```go 
//注册一个路由器
r := gin.Default()
//加载静态文件
r.Static("/xxx", "./statics")
//gin框架中给模板添加自定义函数
r.SetFuncMap(template.FuncMap{
	"safe": func(str string) template2.HTML {
		return template2.HTML(str)
	},
})
//gin框架加载相对路径"./templates"下所有的一级二级文件
r.LoadHTMLGlob("templates/**/*")
//注册路由处理
r.GET("/posts/index", func(c *gin.Context) {
	c.HTML(http.StatusOK, "posts/index.tmpl", gin.H{
		"title": "<a href=\"https://www.nbclass.icu\">posts/index<a>",
	})
})
//运行
r.Run(":9090")
```
#### gin返回json及获取各种参数语法
```go
r := gin.Default()
//返回json
r.GET("/json", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"name": "啊z",
		})
	})
//获取queryString参数
r.GET("/query", func(c *gin.Context) {
		//请求：http://127.0.0.1:9090/query?name=啊z
		name := c.Query("name")
	})
//获取form参数
r.GET("/form", func(c *gin.Context) {
		name := c.PostForm("name")
	})
//获取url路径参数
r.GET("/param/:year/:month", func(c *gin.Context) {
		year := c.Param("year")
		month :+ c.Param("month")
	})
```
#### gin参数绑定
```go
r.GET("/user", func(c *gin.Context) {
		//username := c.Query("username")
		//password := c.Query("password")
		//U := UserInfo{
		//	Username: username,
		//	Password: password,
		//}
		var u UserInfo //声明一个userinfo类型的变量u
		err := c.ShouldBind(&u) //参数绑定
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			fmt.Println(u)
			c.JSON(http.StatusOK, gin.H{
				"message": "ok",
			})
		}

	})
```
#### gin文件上传
```go
r.POST("/upload", func(c *gin.Context) {
		//1、从请求中读取文件，单个文件上传
		//处理multipart forms 提交文件时默认的内存限制是32MiB
		//可以通过下面的方式修改
		//router.MaxMultipartMemory = 8 << 20 // 8MiB
		file, err := c.FormFile("f1")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
		} else {
			//将读取到的文件保存在服务端本地
			//dst := fmt.Sprintf("./%s", file.Filename)
			dst := path.Join("./", file.Filename) //接收多个字符串拼接返回一个字符串
			err := c.SaveUploadedFile(file, dst)
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"message": err.Error(),
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status": "ok",
				})
			}
		}
	})
```
#### gin请求重定向
```go
r.GET("/index", func(c *gin.Context) {
		//c.JSON(http.StatusOK, gin.H{
		//	"status": "OK",
		//})
		//跳转到baidu.com
		c.Redirect(http.StatusMovedPermanently, "https://www.baidu.com")
	})
	r.GET("a", func(c *gin.Context) {
		//跳转到b对应的路由处理函数
		c.Request.URL.Path = "/b" //把请求的url修改
		r.HandleContext(c)        //继续后续的处理
	})
	r.GET("b", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "b",
		})
	})
```
#### gin路由和路由组
上文中的类似于get、post请求都可以称之为路由
```go
//访问get请求走这一条处理逻辑
//路由
r.GET("/index", func(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "GET",
	})
})
//路由组   多用于区分不同的业务线或API版本
//把公用的前缀提取出来，创建一个路由组
videoGroup := r.Group("/video")
{
	videoGroup.GET("/index", func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"msg": "video/index",
		})
	})
	videoGroup.GET("/xx", func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"msg": "video/xx",
		})
	})
}
```
#### gin中间件
```go
// 定义一个中间件m1,统计请求处理函数的耗时
func m1(c *gin.Context) {
	fmt.Println("m1 in...")
	//计时
	start := time.Now()
	//go funcxx(c.Copy()) //在中只能使用c的拷贝
	c.Next() //调用后续的处理函数
	//c.Abort() //阻止调用后续的处理函数
	cost := time.Since(start)
	fmt.Println("cost:", cost)
	fmt.Println("m1 out...")
}
```
全局注册中间件函数m1 <code> r.Use(m1, m2) </code> 
单个路由注册中间件函数 <code> r.GET("index", indexHandler) </code>
<b>路由组注册使用中间件函数</b>:

```go
//路由组中间件方法1：
	group1 := r.Group("/xx", authMiddleWare(true))
	{
		group1.GET("/test1", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group",
			})
		})
		group1.GET("/test2", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group",
			})
		})
	}
	//路由组中间件方法2：
	group2 := r.Group("/xx2")
	group2.Use(authMiddleWare(true))
	{
		group2.GET("/test1", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group2",
			})
		})
		group2.GET("/test2", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "group2",
			})
		})
	}
```
## GORM
ORM全称 Object Relational Mapping，译为“对象关系映射”，它解决了对象和关系型数据库之间的数据交互问题。
使用面向对象编程时，数据很多时候都存储在对象里面，具体来说是存储在对象的各个属性（也称成员变量）中。例如有一个 User 类，它的 id、username、password、email 属性都可以用来记录用户信息。当我们需要把对象中的数据存储到数据库时，按照传统思路，就得手动编写 SQL 语句，将对象的属性值提取到 SQL 语句中，然后再调用相关方法执行 SQL 语句。


获取gorm <code>go get -u gorm.io/gorm </code>

连接数据库（需要连接哪个数据库则导入对应的包即可）：
```go
import _ "github.com/jinzhu/gorm/dialects/mysql"
// import _ "github.com/jinzhu/gorm/dialects/postgres"
// import _ "github.com/jinzhu/gorm/dialects/sqlite"
// import _ "github.com/jinzhu/gorm/dialects/mssql"

```
### GORM入门
```go
package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type UserInfo struct {
	ID     uint
	Name   string
	Gender string
	Hobby  string
}

func main() {
	//连接MySQL数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormDB?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	//创建表，自动迁移（把结构体和数据表进行对应）
	db.AutoMigrate(&UserInfo{})

	//创建数据行
	//u1 := UserInfo{
	//	ID:     1,
	//	Name:   "小米",
	//	Gender: "男",
	//	Hobby:  "游泳",
	//}
	//db.Create(&u1)
	//查询
	var u UserInfo
	db.First(&u) //查询表中第一条数据保存到 u 中
	fmt.Println(u)
	//更新
	db.Model(&u).Update("hobby", "双色球")
	//删除
	db.Delete(&u)
}

```
### GORM数据库建表相关操作

```go
package main

import (
	"database/sql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
)

type UserInfo struct {
	ID     uint
	Name   string
	Gender string
	Hobby  string
}

/* gorm.Model
type Model struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}
*/

// User 定义模型
type User struct {
	gorm.Model
	Name         string
	Age          sql.NullInt64 //零值类型
	BirthDay     *time.Time
	Email        string  `gorm:"type:varchar(100);unique_index"` //设置邮箱，且唯一索引
	Role         string  `gorm:"size:255"`                       //设置字段大小为255
	MenberNumber *string `gorm:"unique;not null"`                //设置会员号 member number 唯一且不为空
	Num          int     `gorm:"AUTO_INCREMENT"`                 //设置num为自增类型
	Address      string  `gorm:"index.addr"`                     //给address字段创建名为addr的索引
	IgnoreMe     int     `gorm:"_"`                              //忽略本字段
	//还可以使用 tag 来指定列名： `gorm:"column:column_name"`
}

// 使用 AnimalID 作为主键
type Animal struct {
	AnimalID int64 `gorm:"primary_key"`
	Name     string
	Age      int64
}

// 唯一指定表名
func (Animal) TableName() string {
	return "animal"
}
func main() {
	//GORM还支持更改默认表名称规则
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return "SMS_" + defaultTableName
	}
	//连接MySQL数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormDB?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	//禁用创建表时的 复数表名
	db.SingularTable(true)

	//创建表，自动迁移（把结构体和数据表进行对应）
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Animal{})
	//使用User结构体创建名叫yonghu的表
	db.Table("yonghu").CreateTable(&User{})

}

```

## CURD
### 创建及默认字段
定义模型
```go
type UserInfo struct {
	ID     uint
	Name   string
	Age  string
}
```
使用NewRecord()查询主键是否存在，主键为空使用Create()创建记录：
```go
user := User{Name: "q1mi", Age: 18}

db.NewRecord(user) // 主键为空返回`true`
db.Create(&user)   // 创建user
db.NewRecord(user) // 创建`user`后返回`false`
```
上面代码实际执行的SQL语句是INSERT INTO users("age") values('99');，排除了零值字段Name，而在数据库中这一条数据会使用设置的默认值小王子作为Name字段的值。

*** 注意 ***所有字段的零值, 比如0, "",false或者其它零值，都不会保存到数据库内，但会使用他们的默认值。 如果你想避免这种情况，可以考虑使用指针或实现 Scanner/Valuer接口，比如：
  
使用指针方式：
```go
// 使用指针
type User struct {
  ID   int64
  Name *string `gorm:"default:'小王子'"`
  Age  int64
}
user := User{Name: new(string), Age: 18))}
db.Create(&user)  // 此时数据库中该条记录name字段的值就是''
```
使用Scanner/Valuer 接口方式：
```go
// 使用 Scanner/Valuer
type User struct {
	ID int64
	Name sql.NullString `gorm:"default:'小王子'"` // sql.NullString 实现了Scanner/Valuer接口
	Age  int64
}
user := User{Name: sql.NullString{"", true}, Age:18}
db.Create(&user)  // 此时数据库中该条记录name字段的值就是''
```
### 查询

```go
package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// 1、定义模型
type User struct {
	gorm.Model
	Name string
	Age  int64
}

func main() {
	//连接mysql数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormdb?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	//2、把模型与数据库中的表对应起来
	db.AutoMigrate(&User{})

	//3、创建
	//u1 := User{
	//	Name: "xiaozhu",
	//	Age:  18,
	//}
	//db.Create(&u1)
	//u2 := User{
	//	Name: "maogou",
	//	Age:  13,
	//}
	//db.Create(&u2)
	//4、查询
	var user User //声明模型结构体类型变量user （文件夹A）
	//user := new(User) //new和make的区别
	//根据主键查询第一条记录
	//db.First(&user)
	//select * from users order by id limit 1;
	var users []User
	//查询所有的记录
	//db.Debug().Find(&users)
	//随机获取一条记录
	//db.Take(&user)
	//获取最后一条记录
	//db.Last(user)
	//根据主键检索
	//db.First(&user, 2) //select * from users where id = 2;
	//db.First(&user, "2")
	db.Find(&users, []int{1, 2}) //select * from users where id in (1,2);
	fmt.Println(users)
	//当目标对象有一个主要值时，将使用主键构建条件，例如：
	user.ID = 2
	db.First(&user)
	fmt.Println(user)
}

```
#### string 条件
```go
// Get first matched record
db.Where("name = ?", "jinzhu").First(&user)
// SELECT * FROM users WHERE name = 'jinzhu' ORDER BY id LIMIT 1;

// Get all matched records
db.Where("name <> ?", "jinzhu").Find(&users)
// SELECT * FROM users WHERE name <> 'jinzhu';

// IN
db.Where("name IN ?", []string{"jinzhu", "jinzhu 2"}).Find(&users)
// SELECT * FROM users WHERE name IN ('jinzhu','jinzhu 2');

// LIKE
db.Where("name LIKE ?", "%jin%").Find(&users)
// SELECT * FROM users WHERE name LIKE '%jin%';

// AND
db.Where("name = ? AND age >= ?", "jinzhu", "22").Find(&users)
// SELECT * FROM users WHERE name = 'jinzhu' AND age >= 22;

// Time
db.Where("updated_at > ?", lastWeek).Find(&users)
// SELECT * FROM users WHERE updated_at > '2000-01-01 00:00:00';

// BETWEEN
db.Where("created_at BETWEEN ? AND ?", lastWeek, today).Find(&users)
// SELECT * FROM users WHERE created_at BETWEEN '2000-01-01 00:00:00' AND '2000-01-08 00:00:00';

```
#### Struct & Map 条件
```go
// Struct
db.Where(&User{Name: "jinzhu", Age: 20}).First(&user)
// SELECT * FROM users WHERE name = "jinzhu" AND age = 20 ORDER BY id LIMIT 1;

// Map
db.Where(map[string]interface{}{"name": "jinzhu", "age": 20}).Find(&users)
// SELECT * FROM users WHERE name = "jinzhu" AND age = 20;

// Slice of primary keys
db.Where([]int64{20, 21, 22}).Find(&users)
// SELECT * FROM users WHERE id IN (20, 21, 22);
```
#### 指定结构体查询字段
```go
db.Where(&User{Name: "jinzhu"}, "name", "Age").Find(&users)
// SELECT * FROM users WHERE name = "jinzhu" AND age = 0;

db.Where(&User{Name: "jinzhu"}, "Age").Find(&users)
// SELECT * FROM users WHERE age = 0;
```
#### 内联条件
```go
// Get by primary key if it were a non-integer type
db.First(&user, "id = ?", "string_primary_key")
// SELECT * FROM users WHERE id = 'string_primary_key';

// Plain SQL
db.Find(&user, "name = ?", "jinzhu")
// SELECT * FROM users WHERE name = "jinzhu";

db.Find(&users, "name <> ? AND age > ?", "jinzhu", 20)
// SELECT * FROM users WHERE name <> "jinzhu" AND age > 20;

// Struct
db.Find(&users, User{Age: 20})
// SELECT * FROM users WHERE age = 20;

// Map
db.Find(&users, map[string]interface{}{"age": 20})
// SELECT * FROM users WHERE age = 20;

```
#### Not 条件
```go
db.Not("name = ?", "jinzhu").First(&user)
// SELECT * FROM users WHERE NOT name = "jinzhu" ORDER BY id LIMIT 1;

// Not In
db.Not(map[string]interface{}{"name": []string{"jinzhu", "jinzhu 2"}}).Find(&users)
// SELECT * FROM users WHERE name NOT IN ("jinzhu", "jinzhu 2");

// Struct
db.Not(User{Name: "jinzhu", Age: 18}).First(&user)
// SELECT * FROM users WHERE name <> "jinzhu" AND age <> 18 ORDER BY id LIMIT 1;

// Not In slice of primary keys
db.Not([]int64{1,2,3}).First(&user)
// SELECT * FROM users WHERE id NOT IN (1,2,3) ORDER BY id LIMIT 1;
```
#### Or 条件
```go
db.Where("role = ?", "admin").Or("role = ?", "super_admin").Find(&users)
// SELECT * FROM users WHERE role = 'admin' OR role = 'super_admin';

// Struct
db.Where("name = 'jinzhu'").Or(User{Name: "jinzhu 2", Age: 18}).Find(&users)
// SELECT * FROM users WHERE name = 'jinzhu' OR (name = 'jinzhu 2' AND age = 18);

// Map
db.Where("name = 'jinzhu'").Or(map[string]interface{}{"name": "jinzhu 2", "age": 18}).Find(&users)
// SELECT * FROM users WHERE name = 'jinzhu' OR (name = 'jinzhu 2' AND age = 18);
```
#### 选择特定字段
```go
db.Select("name", "age").Find(&users)
// SELECT name, age FROM users;

db.Select([]string{"name", "age"}).Find(&users)
// SELECT name, age FROM users;

db.Table("users").Select("COALESCE(age,?)", 42).Rows()
// SELECT COALESCE(age,'42') FROM users;
```
#### 排序
```go
db.Order("age desc, name").Find(&users)
// SELECT * FROM users ORDER BY age desc, name;

// Multiple orders
db.Order("age desc").Order("name").Find(&users)
// SELECT * FROM users ORDER BY age desc, name;

db.Clauses(clause.OrderBy{
  Expression: clause.Expr{SQL: "FIELD(id,?)", Vars: []interface{}{[]int{1, 2, 3}}, WithoutParentheses: true},
}).Find(&User{})
// SELECT * FROM users ORDER BY FIELD(id,1,2,3)
```
### 更新
```go
package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// 1、定义模型
type User struct {
	gorm.Model
	Name   string
	Age    int64
	Active bool
}

func main() {
	//连接mysql数据库
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/gormdb?charset=utf8mb4&parseTime=true&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	//2、把模型与数据库中的表对应起来
	db.AutoMigrate(&User{})

	//3、创建
	//u1 := User{
	//	Name:   "xiaozhu",
	//	Age:    18,
	//	Active: true,
	//}
	//db.Create(&u1)
	//u2 := User{
	//	Name:   "maogou",
	//	Age:    13,
	//	Active: false,
	//}
	//db.Create(&u2)
	//4、查询
	var user User
	db.First(&user)
	var users []User
	db.Find(&users)
	//5、更新
	user.Name = "xiaoai"
	user.Age = 29
	db.Save(&user) //save默认会修改所有字段
	//	//UPDATE `users` SET `created_at` = '2022-11-27 13:56:23', `updated_at` = '2022-12-04 16:07:20', `deleted_at` = NULL, `name` = 'xiaoai', `age` = 29, `active` = true  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 5
	//更新单个属性
	db.Model(&user).Update("name", "woshinidie")
	//根据给定的条件更新单个属性
	db.Model(&users).Debug().Where("active=?", false).Update("active", true)
	//使用map更新多个属性，只会更新其中有变化的属性
	db.Model(&user).Updates(map[string]interface{}{"name": "hello", "age": 18, "active": false})
	//使用struct更新多个属性，只会更新其中有变化且为非零值的字段
	db.Model(&user).Updates(User{
		Name:   "Xixi",
		Age:    21,
		Active: true,
	})
	//注意：当使用struct更新时，GORM只会更新那些非零值的字段
	//对于下面的操作，不会发生任何更新，"",0,false 都是其类型的零值
	db.Model(&user).Updates(User{
		Name:   "",
		Age:    0,
		Active: false,
	})
}
```
#### 更新选定的字段

```go
//更新选定字段
//如果想更新或忽略某些字段，可以使用Select , Omit
//指定更新
db.Model(&user).Select("name").Updates(map[string]interface{}{"name": "小艾同学", "age": 18, "active": false})
//忽略更新
db.Model(&user).Omit("name").Updates(map[string]interface{}{"name": "小艾同学", "age": 18, "active": false})
```
#### 无Hooks更新
上面的更新操作会自动运行model的BeforeUpdate，AfterUpdate 方法，更新UpdateAt时间戳，在更新时保存其 Associations，如果你不想调用这些方法，你可以使用UpdateColumn,UpdateColumns
```go
//更新单个属性，类似于 'Update'
db.Model(&user).UpdateColumn("name", "hello")
//更新多个属性，类似于 'Updates'
db.Model(&user).UpdateColumns(User{
	Name: "Neo",
	Age:  15,
})
```

#### 批量更新
批量更新时 Hooks(钩子函数) 不会进行
```go
//批量更新
db.Table("users").Where("id IN(?)", []int{1, 2}).Updates(map[string]interface{}{"name": "hello", "age": 12})
//使用struct更新时，只会更新非零值字段，若想更新所有字段，请使用map[string]interface{}
db.Model(User{}).Updates(User{
	Name:   "hello",
	Age:    18,
	Active: false,
})
//使用 'RowsAffected' 获取更新记录总数
rows := db.Model(User{}).Updates(User{Name: "hello", Age: 19}).RowsAffected
fmt.Println(rows)
```
#### 使用SQL表达式进行更新
先查询表中的第一条数据保存至 user 变量
```go
var user User
db.First(&user)
```
```go
db.Model(&user).Debug().Update("age", gorm.Expr("age * ? + ?", 2, 100))
//UPDATE `users` SET `age` = age * 2 + 100, `updated_at` = '2022-12-04 16:40:16'  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 1
db.Model(&user).Debug().Updates(map[string]interface{}{"age": gorm.Expr("age * ? + ?", 2, 100)})
//UPDATE `users` SET `a ge` = age * 2 + 100, `updated_at` = '2022-12-04 16:49:23'  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 1
db.Model(&user).Debug().UpdateColumn("age", gorm.Expr("age - ?", 1))
//UPDATE `users` SET `age` = age - 1  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 1
db.Model(&user).Debug().Where("age > 10").UpdateColumn("age", gorm.Expr("age - ?", 1))
//UPDATE `users` SET `age` = age - 1  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 1 AND ((age > 10))
```
#### 修改Hooks中的值
如果想修改 BeforeUpdate ,BeforeSave 等 Hooks 中更新的值，可以使用 scope.SetColumn
```go
func (u *User) BeforeSave(scope *gorm.Scope) (err error) {
	if pw, err := bcrypt.GenerateFromPassword(u.Password, 0); err == nil {
		err := scope.SetColumn("EncryptedPassword", pw)
		if err != nil {
			return err
		}
	}
	return nil
}
```
#### 其他更新选项
```go
//为 updage SQL 添加其他的SQL
db.Model(&user).Set("gorm:update_option", "OPTION(OPTIMIZE FOR UNKNOWN)").Update("name", "hello")
//UPDATE users SET name='hello',updated_at = '2022-12-04 16:07:20' WHERE id = 1 OPTION(OPTIMIZE FOR UNKNOWN);
```

### 删除

#### 删除记录
注意：删除记录时，请确保主键字段有值，GORM会通过主键取删除记录，如果主键为空，GORM会删除该model的所有记录
```go
var user User
user.ID = 6
//删除现有记录(软删除，记录删除时间从而保护数据)
db.Debug().Delete(&user)
//UPDATE `users` SET `deleted_at`='2022-12-08 12:43:45'  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 6
//为删除SQL添加额外的SQL操作
db.Set("gorm:delete_option", "OPTION(OPTIMIZE FOR UNKNOWN)").Delete(&user)
//UPDATE `users` SET `deleted_at`='2022-12-08 12:43:45'  WHERE `users`.`deleted_at` IS NULL AND `users`.`id` = 6 OPTION (OPTIMIZE FOR UNKNOWN)
```
#### 批量删除
删除全部匹配的数据
```go
db.Where("name=?", "hello").Delete(User{})
db.Delete(User{}, "age = ?", 18)
```
#### 软删除
如果一个model有DeletedAt字段，他将自动获得软删除的功能，当调用delete方法时，记录不会真正的从数据库中被删除，只会将DeletedAt 字段的值设置为当前时间	
```go
db.Delete(&user)
//批量删除
db.Where("name=?", "hello").Delete(User{})
//查询记录时会忽略被软删除的记录
db.where("age=20").Find(&user)
//Unscoped 方法可以查询被软删除的记录
db.Unscoped().Where("age=20").Find(&users)
```
#### 物理删除
```go
//Unscoped 方法可以物理删除记录
db.Unscoped().Delete(&order)
//DELETE FROM orders WHERE id=10;
```

### JWT使用
使用 <code>go get github.com/dgrijalva/jwt-go </code> 安装包
```go
package dao

import (
	"GinEssential/model"
	"github.com/dgrijalva/jwt-go"
	"time"
)

//定义加密的密钥
var jwtKey = []byte("a_secret_cret")

//定义token的claim
type Claims struct {
	UserId uint
	jwt.StandardClaims
}

//发放token的方法
func ReleasToken(user model.User) (string, error) {
	//token过期时间
	expirationTime := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		UserId: user.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(), //token过期时间
			IssuedAt:  time.Now().Unix(),     //token发放时间
			Issuer:    "oceanLearn.tech",     //发放人
			Subject:   "user token",          //主题
		},
	}
	//生成token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	//生成token签名字符串
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

//解析token
func ParseToken(tokenString string) (*jwt.Token, *Claims, error) {
	claims := &Claims{}
	//func将接收解析的token，并返回密钥进行验证
	token, err := jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	return token, claims, err
}

```
在userController中
```go
//发放token给前端
token, err := dao.ReleasToken(user)
if err != nil {
	c.JSON(http.StatusInternalServerError, gin.H{
		"code": 500,
		"msg":  "系统异常！",
	})
	log.Println("token generate err:", err)
	return
}
//返回结果
c.JSON(http.StatusOK, gin.H{
	"code": 200,
	"msg":  "登录成功！",
	"data": gin.H{
		"token": token,
	},
})
```
#### token组成部分
生成的token字符串：
<code>eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjQsImV4cCI6MTY3MDkyNjA5OCwiaWF0IjoxNjcwODM5Njk4LCJpc3MiOiJvY2VhbkxlYXJuLnRlY2giLCJzdWIiOiJ1c2VyIHRva2VuIn0.x7yhWcCNRnyOLXfQ8pZcE0rAIkYZabEjQYH2zL6eGB0</code>

token由三部分组成：
表头：Header是一个json对象，存储元数据
负载：Payload是一个json对象。存放传递的数据，数据分为Public和Private。
Signature：签名。签名是对Header和Payload两部分的签名，目的是为了防止数据被篡改

#### 认证中间件
```go
package middleware

import (
	"GinEssential/dao"
	"GinEssential/model"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func AuthMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		//获取Authorization header
		tokenString := c.GetHeader("Authorization")

		//validate token format
		//strings.HasPrefix 测试字符串是否以指定前缀开头
		//此处踩坑：验证Token前缀时，不断401报错，后发现postman默认前缀为Authorization: Bearer ，故此处前缀应为 " Bearer "，bearer前后均有空格
		if tokenString == "" || strings.HasPrefix(tokenString, " Bearer ") {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": 401,
				"msg":  "权限不足",
			})
			//将本次请求抛弃
			c.Abort()
			return
		}
		tokenString = tokenString[7:]
		token, claims, err := dao.ParseToken(tokenString)
		if err != nil || !token.Valid {
			//此处踩坑：token.Valid 应取反，返回值为真时，token验证成功
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": 401,
				"msg":  "权限不足",
				"err":  err,
			})
			//将本次请求抛弃
			c.Abort()
			return
		}
		//验证通过后获取claim中的userId
		userId := claims.UserId
		var user model.User
		dao.DB.First(&user, userId)
		//用户不存在
		if user.ID == 0 {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": 401,
				"msg":  "用户不存在",
			})
			//将本次请求抛弃
			c.Abort()
			return
		}
		//用户存在，将用户信息写入下文
		c.Set("user", user)
		c.Next()
	}
}

```

### Viper
install: <code> go get github.com/spf13/viper </code>
创建application.yml文件以保存相应参数配置：
```yml
#server:
#  port: 1016
datasource:
  driverName: mysql
  host: 127.0.0.1
  port: 3306
database: essential
  username: root
  password: root
  charset: utf8mb4
```
在main.go中创建初始化函数：
```go
func InitConfig() {
	workDir, _ := os.Getwd()
	//配置文件名
	viper.SetConfigName("application")
	//文件类型
	viper.SetConfigType("yml")
	//文件路径
	viper.AddConfigPath(workDir + "/config")
	//读取配置文件
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
```
使用viper
```go
UserName := viper.GetString("datasource.username")
Password := viper.GetString("datasource.password")
IpAddress := viper.GetString("datasource.host")
Port := viper.GetString("datasource.port")
DbName := viper.GetString("datasource.database")
Charset := viper.GetString("datasource.charset")
driverName := viper.GetString("datasource.driverName")
```
### 搭建前端环境
使用nvm安装和管理node.js ：<code>https://github.com/coreybutler/nvm-windows/releases</code>
#### NVM使用
nvm 版本 1.1.10
```cmd
nvm ls                     		// 查看安装的所有node.js的版本
nvm list available			// 显示可以安装的所有node.js的版本
nvm install 版本号 			// 安装对应的版本，例如：nvm install 14.19.0
nvm use 版本号           		// 切换到使用指定的nodejs版本
node -v					//查看是否切换完成
```
#### yarn
安装yarn ：<a>https://yarnpkg.com/latest.msi</a>
安装完成后： <code>yarn -v</code> 查看版本
#### Vue Cli
##### 安装 
<code>npm install -g @vue/cli</code>
或者：
<code>yarn global add @vue/cli</code>
##### 创建项目
1、使用 create 命令创建项目：<code>vue create gin-vue</code>
2、根据提示，选择需要的功能
3、运行： <code>cd gin-vue
yarn serve</code>

#### Eslint
1、检查语法错误
2、检查代码规范
.eslintrc.js
```js
indent: ["warn", 4], // 缩进 [提示级别，缩进格数]
semi: ["error", "never"], // 每行结束的分号 [提示级别, 是否需要]
quotes: ["error", "double"], // 字符串单双引号 [提示级别, 单双引号(single)]
```
踩坑：CTRL+s 时无法自动格式化代码
解决方法：打开vscode的eslint扩展设置，在settings.json中添加如下代码即可：
```json
{
    "eslint.run": "onType",
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
}
```
#### BootstrapVue
引入项目：
```
# With npm
npm install vue bootstrap-vue bootstrap

# With yarn
yarn add vue bootstrap-vue bootstrap
```
在main.js中添加如下代码
```
// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
```
在assets文件夹下新建scss文件夹，然后创建index.scss文件，引入 Bootstrap 和 BootstrapVue的 scss 文件用于驱动SCSS混合:
```
// import bootstrap
@import '~bootstrap';
@import '~bootstrap-vue';
```
最后在main.js中引入scss文件
```
// main.js
import './assets/scss/index.scss';
```
<b>踩坑：总是saas报错，重新部署仍然如此</b>
<b>解决方法</b>：不使用上述方法引入bootstrap，步骤如下：
1、安装 Bootstrap、 Popper，因为 Bootstrap的下拉菜单、弹出框和工具提示都依赖于 Popper来定位。如果您不打算使用这些组件，则可以在此处省略 Popper。
<code>npm i --save bootstrap @popperjs/core</code>
安装额外的依赖Sass来正确导入和捆绑 Bootstrap 的 CSS。
<code>npm i --save-dev sass</code>
安装图标库
<code>npm i bootstrap-icons</code>
2、引入bootstrap样式文件
在assets目录下创建一个style.scss文件，写入如下代码：
```scss
// Import all of Bootstrap's CSS
@import "bootstrap/scss/bootstrap";
// 图标
@import 'bootstrap-icons/font/bootstrap-icons.css';
```
3、在入口文件main.ts/main.js中引入样式文件和JS文件
```js
import '/src/assets/styles.scss'
import "@popperjs/core";
import "bootstrap";
```
搞定！